/* global require, module */

var EmberApp = require('ember-cli/lib/broccoli/ember-app');

var app = new EmberApp(/*{
    lessOptions: {
        paths: [
            'bower_components/bootstrap/less',
            'app/styles'
        ]
    }
}*/);

// Use `app.import` to add additional libraries to the generated
// output files.
//
// If you need to use different assets in different
// environments, specify an object as the first parameter. That
// object's keys should be the environment name and the values
// should be the asset to use in that environment.
//
// If the library that you are including contains AMD or ES6
// modules that you would like to import into your application
// please specify an object with the list of modules as keys
// along with the exports of each module as its value.
app.import('bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css');

app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.eot', {
    destDir: 'fonts'
});
app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf', {
    destDir: 'fonts'
});
app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.svg', {
    destDir: 'fonts'
});
app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff', {
    destDir: 'fonts'
});
app.import('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2', {
    destDir: 'fonts'
});

app.import('bower_components/bootstrap/dist/js/bootstrap.js');
app.import('bower_components/bootstrap-switch/dist/js/bootstrap-switch.js');

app.import('bower_components/tyrion/dist/assets/pfbase.css');
app.import('bower_components/tyrion/dist/assets/tyrion.css');
app.import('bower_components/tyrion/dist/assets/tyrion.js');

// TODO: remove tyrion deps once we get it as an addon
app.import(app.bowerDirectory + '/kendo-ui-pro/src/styles/web/kendo.common.css');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/styles/web/kendo.default.css');

app.import(app.bowerDirectory + '/prism/prism.js');
app.import(app.bowerDirectory + '/prism/themes/prism-okaidia.css');

app.import(app.bowerDirectory + '/modernizr/modernizr.js');
// app.import(app.bowerDirectory + '/jquery/jquery.js');
app.import(app.bowerDirectory + '/underscore/underscore.js');

// @if dist=false -->
// app.import(app.bowerDirectory + '/handlebars/handlebars.js');
// app.import(app.bowerDirectory + '/ember/ember.js');
// @endif --><!-- @if dist=true -->
// app.import(app.bowerDirectory + '/handlebars/handlebars.js');
// app.import(app.bowerDirectory + '/ember/ember.prod.js');
// @endif -->
// app.import(app.bowerDirectory + '/ember-data/ember-data.js');
// app.import(app.bowerDirectory + '/loader.js');
// app.import(app.bowerDirectory + '/ember-resolver/dist/ember-resolver.js');
// app.import(app.bowerDirectory + '/ic-ajax/main.js');
app.import(app.bowerDirectory + '/lodash/dist/lodash.js');
app.import(app.bowerDirectory + '/sticky/jquery.sticky.js');
app.import(app.bowerDirectory + '/prettycheckable/dev/prettyCheckable.js');
app.import(app.bowerDirectory + '/bootstrap/dist/js/bootstrap.js');
app.import(app.bowerDirectory + '/bootstrap-datepicker/js/bootstrap-datepicker.js');
app.import(app.bowerDirectory + '/moment/moment.js');
// TODO: add reference back once we have the contentForHead for the gmaps ref
// app.import(app.bowerDirectory + '/gmaps/gmaps.js');
// TODO: add mousewheel and antiscroll back once we know where we were getting it from before ember-cli
// They were both 404ing
// app.import(app.bowerDirectory + '/jquery-mousewheel/jquery.mousewheel.js');
// app.import(app.bowerDirectory + '/antiscroll/antiscroll.js');
app.import(app.bowerDirectory + '/autoNumeric/autoNumeric.js');
app.import(app.bowerDirectory + '/keymaster/keymaster.js');
// Kendo Grid and its dependencies: http://docs.telerik.com/kendo-ui/getting-started/javascript-dependencies-->
// NOTE: Using kendo's unminified files for now since SourceMaps don't work -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.core.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.data.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.columnsorter.js');

// Editing, filtering, column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.calendar.js');
// Editing, Filtering, Column menu, Row filter, Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.popup.js');
// Editing, Filtering, Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.datepicker.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.timepicker.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.datetimepicker.js');
// Editing, Filtering, Column menu, Grouping, Selection, Column reordering, Column resizing, Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.userevents.js');
// Editing, Filtering, Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.numerictextbox.js');
// Editing -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.validator.js');
// Editing, Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.binder.js');
// Editing -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.editable.js');
// Editing, Grouping, Column reordering, Column resizing, Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.draganddrop.js');
// Editing -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.window.js');
// Filtering, Column menu, Row filter -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.list.js');
// Filtering, Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.dropdownlist.js');
// Filtering, Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.filtermenu.js');
// Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.menu.js');
// Column menu -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.columnmenu.js');
// Grouping -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.groupable.js');
// Row filter -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.autocomplete.js');
// Row filter -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.filtercell.js');
// Paging -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.pager.js');
// Selection -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.selectable.js');
// Column reordering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.reorderable.js');
// Column resizing -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.resizable.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.fx.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.scroller.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.view.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.view.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.loader.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.pane.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.router.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.application.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.popover.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.shim.js');
// Grid adaptive rendering -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.mobile.actionsheet.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.grid.js');

// Kendo MaskedTextBox (and NumericTextBox) used for input fields -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.maskedtextbox.js');

// Kendo ColorPicker Dependencies -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.slider.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.color.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.colorpicker.js');

app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.upload.js');

// Kendo Editor Dependencies -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.editor.js');

// And the rest... -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.multiselect.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.window.js');

// Scheduler Components -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.view.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.agendaview.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.dayview.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.monthview.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.recurrence.js');
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.scheduler.js');

app.import(app.bowerDirectory + '/wysihtml/dist/wysihtml5x-toolbar.js');
app.import(app.bowerDirectory + '/wysihtml/parser_rules/advanced.js');

// AutoComplete Dependencies. The following dependencies were specified above jquery.js
// kendo.core.js
// kendo.data.js
// kendo.popup.js
// kendo.list.js
// kendo.fx.js (Mobile scroller feature)
// kendo.userevents.js (Mobile scroller feature)
// kendo.draganddrop.js (Mobile scroller feature)
// kendo.mobile.scroller.js (Mobile scroller feature) -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.autocomplete.js');
// endbuild -->

// Kendo Sortable -->
app.import(app.bowerDirectory + '/kendo-ui-pro/src/src/kendo.sortable.js');



module.exports = app.toTree();