import Ember from 'ember';
import WithComponentsMixin from '../../../mixins/with-components';
import { module, test } from 'qunit';

module('WithComponentsMixin');

// Replace this with your real tests.
test('it works', function(assert) {
  var WithComponentsObject = Ember.Object.extend(WithComponentsMixin);
  var subject = WithComponentsObject.create();
  assert.ok(subject);
});
