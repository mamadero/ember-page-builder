# Pf-page-builder

The goal of the Pf-page-builder is to give designers a tool to be able rapidly prototype a page layout based on PF's responsive framework. The tool would allow designers to drag and drop containers (grid rows and columns) into a page and populate them with Tyrion components.  The PFPageBuilder would give designers a realtime view of a responsive layout across all screen sizes.

How-to
Drag components from the Tool Box on the left into the EHR canvas.  To edit a component, mouse over the component in the canvas and click the 'info' icon that appears.  Edit the component's properties in the Property Inspector on the right.  To delete your component, mouse over the component in the canvase and click the 'x' icon that appears.  Start with a Grid Row component, adjust the column layout in the Property Inspector and populate the columns with other components or other Grid Rows!  To test your page, toggle the 'Build/Test' switch in the top left corner of the page.

###Credits
* [Quinn Hoyer](https://stash.hq.practicefusion.com/users/qhoyer) for the original idea
* [Arnold Sandoval](https://stash.hq.practicefusion.com/users/asandoval) for CSS and feedback

## Prerequisites

You will need the following things properly installed on your computer.

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Bower](http://bower.io/)
* [Ember CLI](http://www.ember-cli.com/)
* [PhantomJS](http://phantomjs.org/)

## Installation

* `git clone <repository-url>` this repository
* change into the new directory
* `npm install`
* `bower install`

## Running / Development

* `ember server`
* Visit your app at [http://localhost:4200](http://localhost:4200).

### Code Generators

Make use of the many generators for code, try `ember help generate` for more details

### Running Tests

* `ember test`
* `ember test --server`

### Building

* `ember build` (development)
* `ember build --environment production` (production)

### Deploying

Specify what it takes to deploy your app.

## Further Reading / Useful Links

* [ember.js](http://emberjs.com/)
* [ember-cli](http://www.ember-cli.com/)
* Development Browser Extensions
  * [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  * [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)

