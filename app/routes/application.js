import Ember from 'ember';

export default Ember.Route.extend({
	isEmbeddedInIframe: false,

	beforeModel: function(transition) {
		this.set('isEmbeddedInIframe', !Ember.isEmpty(transition.params['page.embed']));
	},

	setupController: function (controller, model) {
		this._super(controller, model);
		controller.set('isEmbeddedInIframe', this.get('isEmbeddedInIframe'));
	}
});
