import Ember from 'ember';

export default Ember.Route.extend({
	/**	This route is used to embed PfPageBuilder in an iframe.
	 *	We do this so that we can toggle different widths to preview a page's responsive layout
	 *	We are only able to honor the CSS media queries by embedding in an iframe.
	 *
	 *	NOTE:	This solution kind of presents a problem, by re-rendering the app in an iframe we lose
	 *			the ability to use the ember inspector to debug the app.  Lame.
	 */
});
