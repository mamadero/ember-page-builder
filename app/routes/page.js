import Ember from 'ember';

export default Ember.Route.extend({
	model: function(params, transition) {
		return this.store.find('page', params.page_id);
	},

	actions: {
		error: function(error, transition) {
			// Assuming we got here due to the error in `beforeModel`,
			// we can expect that error === "bad things!",
			// but a promise model rejecting would also
			// call this hook, as would any errors encountered
			// in `afterModel`.

			// The `error` hook is also provided the failed
			// `transition`, which can be stored and later
			// `.retry()`d if desired.

			this.transitionTo('index');
		}
	}
});
