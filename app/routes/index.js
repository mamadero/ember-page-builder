import Ember from 'ember';

export default Ember.Route.extend({
	redirect: function() {
		var page = this.store.createRecord('page', {
			timestamp: new Date().getTime()
		});
		page.save().then(function() {
			this.transitionTo('page', page);
		}.bind(this));
	}
});
