import Ember from 'ember';

export default Ember.Mixin.create({
	
    components: null,
    config: null,
    model: null,
    pageController: null,
    areComponentsLoaded: false,
    areConfigsLoaded: false,

    /* Called on 'init', fetches components and configs and draws the grid/row/columns */
    onInit: function() {
        this.set('pageController', this.get('container').lookup('controller:page'));
        try {
            this.get('model.components').then(function(components) {
                this.set('components', components);
                this.set('areComponentsLoaded', true);
            }.bind(this));
        } catch (error) {
            console.log('There are no nested components to load for this model.');
        }
        try {
            this.get('model.config').then(function(config) {
                this.set('config', config);
                this.set('areConfigsLoaded', true);
            }.bind(this));
        } catch (error) {
            console.log('There are no config objects for this model.');
        }
    }.on('init'),

    destroyComponent: function(component) {
        if (component.get('currentState.stateName') === 'root.deleted.inFlight') {
            return;
        }
        // destroy the component record
        component.destroyRecord().then(function(destroyedComponent) {
            // then destroy all its related configs
            destroyedComponent.get('config').then(function(config) {
                config.forEach(function(componentConfig) {
                    Ember.run.once(this, function() {
                        componentConfig.destroyRecord();
                    });
                });
            });
            // then destroy any component records that may be nested
            destroyedComponent.get('components').then(function(components) {
                components.forEach(function(childComponentOfDestroyedComponent) {
                    Ember.run.once(this, function() {
                        this.destroyComponent(childComponentOfDestroyedComponent);
                    });
                }.bind(this));
            }.bind(this));
        }.bind(this)).catch(function(error) {
            console.log(error.message);
        }).finally(function() {
            // finally save then reload the model
            // this.get('model').save();
        }.bind(this));
    },

    actions: {
        editComponent: function() {
            // HACK:    This is probably bad and I should go with 'data down/actions up' instead
            //          But I was having trouble sending actions up from deeply nested components to the pageController
            this.get('pageController').send('editComponent', this);
        }
    }

});
