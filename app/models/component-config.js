import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
	name: DS.attr('string'),
	value: DS.attr(),
	options: DS.attr('string'),
	type: DS.attr('string', {defaultValue: 'text'}),//text, select, checkbox, textarea
	component: DS.belongsTo('component', {async: true}),
	page: DS.belongsTo('page', {async: true}),
	isText: Ember.computed.equal('type', 'text'),
	isSelect: Ember.computed.equal('type', 'select'),
	isCheckBox: Ember.computed.equal('type', 'checkbox'),
	isTextArea: Ember.computed.equal('type', 'textarea'),
	selectOptions: function() {
		if (Ember.isEmpty(this.get('options'))) {
			return null;
		}
		return this.get('options').split('|');
	}.property('options')
});
