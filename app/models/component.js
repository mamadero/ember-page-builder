import DS from 'ember-data';

export default DS.Model.extend({
	name: DS.attr('string'),
	title: DS.attr('string'),
	config: DS.hasMany('component-config', {async: true}),
	components: DS.hasMany('component', {async: true}),
	component: DS.belongsTo('component', {inverse: 'components', async: true}),
	page: DS.belongsTo('page', {async: true})
});
