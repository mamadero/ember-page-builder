/**
 * Page model
 * @constructor
 */
import DS from 'ember-data';

export default DS.Model.extend({
	timestamp: DS.attr('number'),
	components: DS.hasMany('component', {async: true})
});
