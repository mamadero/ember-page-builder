/**
 * ToolBar component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	classNames: ['component-toolbar', 'hidden-xs', 'container']
});
