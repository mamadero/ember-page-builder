/**
 * Header component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend();
