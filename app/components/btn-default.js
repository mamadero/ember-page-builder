/**
 * Primary button component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
    type: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'type').get('value');
        }
    }.property('config.@each.value')
});