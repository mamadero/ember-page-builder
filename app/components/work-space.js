/**
 * WorkSpace component that contains the main EHR chrome where DraggableItems are dropped
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
	model: null,
    classNames: ['work-space'],
    classNameBindings: ['isBuildState:col-sm-8:col-sm-12']
});