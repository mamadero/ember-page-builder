/**
 * Represents a Bootstrap row
 * @constructor
 */
import Ember from 'ember';
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
    classNames: ['row'],
    cssClasses: null, // overrides computed property from _super
    copy: null, // overrides computed property from _super

    areConfigsLoadedObserver: function() {
        if (this.get('areConfigsLoaded')) {
            var columns = this.get('columns'),
                columnsConfig = this.get('columnsConfig');
            this.drawGridRowColumns(columns);
        }
    }.observes('areConfigsLoaded'),

    /* Computed property array of columns in the row */
    columns: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'columns').get('value').split('-');
        }
    }.property('config.@each.value'),

    columnsConfig: function() {
        return this.get('config').findBy('name', 'columns');
    }.property('config'),

    /* Observer called when the columns array changes */
    columnsObserver: function() {
        //Note:     Something weird is happening with this observer, 
        //          when the columns config is changed via PropertyInspectorComponent this observer is triggered,
        //          but it is triggered again when we save this model, not sure why but could be related to:
        //          https://github.com/emberjs/data/issues/2937
        var columns = this.get('columns'),
            gridRowColumn;

        // if the columns array is empty, that means the config has not been loaded yet
        if (Ember.isEmpty(columns)) {
            return;
        }

        // Retrieve the list of components
        this.get('model.components').then(function(components) {
            // if the number of columns matches the number of components, just die here
            // this is the hack in order to prevent the observer from firing multiple times as stated above
            // TODO: Fix a bug where no update is made when switching from columns of the same number ie. 6-6 to 8-4
            // if (columns.get('length') === components.get('length')) {
            //     return;
            // }

            // if there are more columns than GridRowColumn components, then add some GridRowColumn components
            if (columns.get('length') > components.get('length')) {
                // add grid row columns
                columns.forEach(function(column, index) {
                    // create GridRowColumn if one is not present at the specified index
                    if (Ember.isEmpty(components.objectAt(index))) {
                        gridRowColumn = this.createGridRowColumn(index);
                        components.pushObject(gridRowColumn);
                        // gridRowColumn.save();
                    } else {
                        // else just update the existing GridRowColumn config
                        components.objectAt(index).get('config').findBy('name', 'columns').set('value', column).save();
                    }
                }.bind(this));
            } else if (columns.get('length') === components.get('length')) {
                components.forEach(function(component, index) {
                    try {
                        component.get('config').findBy('name', 'columns').set('value', columns.objectAt(index)).save();
                    } catch (error) {
                        console.log('Tried to set column value and failed:', error.message);
                    }
                }.bind(this));
            } else {
                // else there are more GridRowColumn components than columns, so remove the unneccessary components
                components.forEach(function(component, index) {
                    // destroy GridRowColumn if a column is not present at the specified index
                    if (Ember.isEmpty(columns.objectAt(index))) {
                        Ember.run.once(this, function() {
                            this.destroyComponent(component);
                        });
                    } else {
                        // else just update the existing GridRowColumn config
                        component.get('config').findBy('name', 'columns').set('value', columns.objectAt(index)).save();
                    }
                }.bind(this));
            }        
        }.bind(this)).finally(function() {
            // finally save this model, this save is what triggers this observer again as stated above
            this.get('model').save();
        }.bind(this));
    }.observes('columns'),

    /* Draws the grid/row/columns */
    drawGridRowColumns: function(columns) {
        var gridRowColumn;
        this.get('model.components').then(function(components) {
            if (Ember.isEmpty(components)) {
                columns.forEach(function(column, index) {
                    gridRowColumn = this.createGridRowColumn(index);
                    components.pushObject(gridRowColumn);
                    gridRowColumn.save();
                }.bind(this));
            }
        }.bind(this)).finally(function() {
            this.get('model').save();
        }.bind(this));
    },

    /* Creates a GridRowColumn record */
    createGridRowColumn: function(index) {
        var gridRowColumn = this.store.createRecord('component', {
            name: 'grid-row-column',
            title: 'Grid Row Column'
        });
        gridRowColumn.get('config').then(function(config) {
            var gridRowColumnConfig = this.store.createRecord('component-config', {
                name: 'columns',
                value: this.get('columns')[index]
            });
            config.pushObject(gridRowColumnConfig);
            gridRowColumnConfig.save();
        }.bind(this)).finally(function() {
            gridRowColumn.save();
        });
        return gridRowColumn;
    }
});