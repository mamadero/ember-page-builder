/**
 * Select component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
    classNames: ['drop-down', 'full-width'],
    
    options: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'options').get('value').split('\n');
        }
    }.property('config.@each.value')
});