/**
 * Represents a Bootstrap column within a Boostrap row that accepts DraggableItems
 * @constructor
 */
import DraggableDropzone from './draggable-dropzone';

export default DraggableDropzone.extend({
	classNameBindings: ['columnClassName'],

	columns: function() {
		if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'columns').get('value').split(',');
        }
	}.property('config.@each.value'),
	
	columnClassName: function() {
		return 'col-sm-' + this.get('columns');
	}.property('columns')
});
