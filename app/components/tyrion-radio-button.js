/**
 * Anchor tag component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	target: function() {
		return this.get('elementId') + 'Target';
	}.property('elementId')
});
