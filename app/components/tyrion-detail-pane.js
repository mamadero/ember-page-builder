/**
 * Detail pane component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	classNames: ['full-width'],
	resizables: [{ element: '.detail-pane-body-wrapper', offset: 320 }],

	footerLeftButtonText: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'footerLeftButtonText').get('value');
        }
    }.property('config.@each.value'),

    footerRightButtonText: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'footerRightButtonText').get('value');
        }
    }.property('config.@each.value'),

    smallHeaderText: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'smallHeaderText').get('value');
        }
    }.property('config.@each.value'),

    bigHeaderText: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'bigHeaderText').get('value');
        }
    }.property('config.@each.value')
});
