/**
 * Button group component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	selection: null,

	options: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'options').get('value').split('\n');
        }
    }.property('config.@each.value'),

	groupOptions: function() {
		var arr;
		if (this.get('options')) {
			arr = [];
			this.get('options').forEach(function(option, index) {
				arr.pushObject({name: option, id: index});
			});
		}
		return arr;
	}.property('options'),

	prompt: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'prompt').get('value');
        }
    }.property('config.@each.value'),
});