/**
 * Confirm dialog component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	buttonText: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'buttonText').get('value');
        }
    }.property('config.@each.value'),

    title: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'title').get('value');
        }
    }.property('config.@each.value'),

    cancel: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'cancel').get('value');
        }
    }.property('config.@each.value'),

    confirm: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'confirm').get('value');
        }
    }.property('config.@each.value')
});
