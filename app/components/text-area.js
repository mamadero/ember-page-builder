/**
 * TextArea component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	classNames: ['full-width'],
	
    placeHolder: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'placeHolder').get('value');
        }
    }.property('config.@each.value')
});