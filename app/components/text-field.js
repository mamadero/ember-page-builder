/**
 * TextField component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
    classNames: ['full-width'],
    
    type: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'type').get('value');
        }
    }.property('config.@each.value'),
    
    placeHolder: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'placeHolder').get('value');
        }
    }.property('config.@each.value')
});