/**
 * Label component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	cssClasses: null,//override
	isRequired: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'isRequired').get('value');
        }
    }.property('config.@each.value'),
});
