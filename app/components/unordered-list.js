/**
 * An unordered list <ul> that accepts DraggableItems
 * @constructor
 */
import DraggableDropzone from './draggable-dropzone';

export default DraggableDropzone.extend({
	classNames: ['component', 'full-width'],

	cssClasses: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'classes').get('value');
        }
    }.property('config.@each.value'),

	actions: {
		deleteComponent: function() {
			this.sendAction('deleteComponent', this);
		}
	}
});
