/**
 * ToolBox sidebar component that contains DraggableItems
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['tool-box', 'sidebar'],
    classNameBindings: ['isBuildState:col-sm-2:hidden'],
    components: null,// list of components to drag/drop in the workspace

    componentsSorting: ['title'],
    sortedComponents: Ember.computed.sort('components', 'componentsSorting'),

    onInit: function() {
        this.getComponents('/assets/components.json').then(function(data) {
            this.set('components', data);
        }.bind(this));
    }.on('init'),

    getComponents: function(url) {
        return new Ember.RSVP.Promise(function(resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onreadystatechange = handler;
            xhr.responseType = 'json';
            xhr.setRequestHeader('Accept', 'application/json');
            xhr.send();
            function handler() {
                if (this.readyState === this.DONE) {
                    if (this.status === 200) {
                        resolve(this.response);
                    } else {
                        reject(new Error('getJSON: `' + url + '` failed with status: [' + this.status + ']'));
                    }
                }
            }
        });
    }
});