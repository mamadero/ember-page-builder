/**
 * Property inspector sidebar that contains selected component properties
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
	classNames:['property-inspector', 'sidebar'],
	classNameBindings: ['isBuildState:col-sm-2:hidden'],
	componentToEdit: null,
	hasComponentToEdit: Ember.computed.notEmpty('componentToEdit'),

	// A little hacky, but when the componentToEdit changes
	// update the class names accordingly
	componentToEditObserver: function() {
		// Eww, turn off the active state on any components
		$('.component').removeClass('active');

		if (this.get('componentToEdit')) {
			// turn on the active state on the componentToEdit
			this.get('componentToEdit').$().addClass('active');
		}
	}.observes('componentToEdit'),

	configObserver: function() {
		if (this.get('hasComponentToEdit')) {
			this.get('componentToEdit.model.config').then(function(config) {
				config.forEach(function(componentConfig) {
					if (componentConfig.get('isDirty')) {
						componentConfig.save();
					}
				});
			});
		}
	}.observes('componentToEdit.model.config.@each.value', 'hasComponentToEdit')
});