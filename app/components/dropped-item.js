/**
 * Represents a component that has been dropped on a DraggableDropzone
 * and has been added to the DraggableDropzone's 'components' array
 * @constructor
 */
import Ember from 'ember';
import WithComponents from '../mixins/with-components';

export default Ember.Component.extend(WithComponents, {
    classNames: ['component'],

    cssClasses: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'classes').get('value');
        }
    }.property('config.@each.value'),

    copy: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'copy').get('value');
        }
    }.property('config.@each.value'),

    mouseEnter: function(event) {
        $('.component').removeClass('hover');
        this.$().addClass('hover');
    },
    
    mouseLeave: function(event) {
        var $toElement = $(event.toElement);
        $('.component').removeClass('hover');
        if ($toElement.hasClass('component')) {
            $toElement.addClass('hover');
        }
    },

    actions: {
        deleteComponent: function() {
            this.get('pageController').send('editComponent', null);
            this.sendAction('deleteComponent', this);
        }
    }
});