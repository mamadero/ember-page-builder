/**
 * Represents a target that can accept DraggableItem components
 * On drop of the DraggableItem, the item's data is pushed to the 'components' array
 * @constructor
 */
import Ember from 'ember';
import WithComponents from '../mixins/with-components';

export default Ember.Component.extend(WithComponents, {
    classNames: ['draggable-dropzone'],
    classNameBindings: ['dragClass'],
    dragClass: 'deactivated',

    dragLeave: function(event) {
        event.preventDefault();
        this.set('dragClass', 'deactivated');
    },

    dragOver: function(event) {
        event.preventDefault();
        this.set('dragClass', 'activated');
    },

    drop: function(event) {
        event.stopPropagation();
        var data = event.dataTransfer.getData('text/data');
        this.send('dropped', data);

        this.set('dragClass', 'deactivated');
    },

    actions: {
        dropped: function(data) {
            var componentData = JSON.parse(data),
                newComponent = this.store.createRecord('component', {
                    name: componentData.name,
                    title: componentData.title
                }),
                newConfig;

            // First get the new component's config array
            newComponent.get('config').then(function(newComponentConfig) {
                // then loop through the config objects in the json data
                componentData.config.forEach(function(config) {
                    // create a new config record
                    newConfig = this.store.createRecord('component-config', {
                        name: config.name,
                        value: config.value,
                        options: config.options,
                        type: config.type
                    });
                    // push the new config record to the config array
                    newComponentConfig.pushObject(newConfig);
                    // save the new config
                    newConfig.save();
                }.bind(this));

                // get the components for this model
                this.get('model.components').then(function(components) {
                    // push the new component
                    components.pushObject(newComponent);
                    // save the new component then save the model
                    newComponent.save();
                }.bind(this)).finally(function() {
                    // save the model
                    this.get('model').save();
                }.bind(this));
            }.bind(this));
        },

        deleteComponent: function(component) {
            this.get('pageController').send('editComponent', null);
            var componentToDestroy = this.get('model.components').findBy('id', component.get('id'));
            this.destroyComponent(componentToDestroy);
        }
    }

});