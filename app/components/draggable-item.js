/**
 * Represents an object that can be dragged from the ToolBox and dropped
 * on a DraggableDropzon target
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
    classNames: ['draggable-item'],
    attributeBindings: ['draggable'],
    draggable: 'true',

    dragStart: function(event) {
        return event.dataTransfer.setData('text/data', JSON.stringify(this.get('content')));
    },

    dragEnd: function() {
        this.$().removeClass('hover');
    },

    mouseEnter: function() {
    	this.$().addClass('hover');
    },

    mouseLeave: function() {
    	this.$().removeClass('hover');
    }
});