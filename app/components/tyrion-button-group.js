/**
 * Button group component
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	options: function() {
        if (Ember.isEmpty(this.get('config'))) {
            return null;
        } else {
            return this.get('config').findBy('name', 'options').get('value').split(',');
        }
    }.property('config.@each.value'),

	groupOptions: function() {
		var arr;
		if (this.get('options')) {
			arr = [];
			this.get('options').forEach(function(option, index) {
				arr.pushObject({label: option, id: index});
			});
		}
		return arr;
	}.property('options')
});
