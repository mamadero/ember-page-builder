/**
 * UtilityOverlay component that contains the 'edit' and 'delete' buttons for DroppedItems
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
	classNames: ['utility-overlay'],
	
	actions: {
		editComponent: function() {
			this.sendAction('editComponent');
		},

		deleteComponent: function() {
			this.sendAction('deleteComponent');
		}
	}
});
