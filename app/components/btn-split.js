/**
 * Primary button component with split dropdown menu
 * @constructor
 */
import DroppedItem from './dropped-item';

export default DroppedItem.extend({
	dropDownCssClasses: function() {
		return this.get('cssClasses') + ' dropdown-toggle split-btn';
	}.property('cssClasses')
});