/**
 * Main navigation bar
 * @constructor
 */
import Ember from 'ember';

export default Ember.Component.extend({
	tagName: 'nav',
	classNames:['navbar', 'navbar-default'],
	isBuildState: false,
	isGridOn: false,

	onDidInsertElement: function() {
		this.$('select#screen-size-select').change(function(event) {
			if ($(this).val() === 'default') {
				// selected 'default'
				$('iframe#pf-page-builder-iframe').attr('class', '');
			} else {
				// selected a numeric value
				$('iframe#pf-page-builder-iframe').attr('class', 'max-width-' + $(this).val());
			}
		});
	}.on('didInsertElement'),

	buildStateObserver: function() {
		if (this.get('isBuildState')) {
			this.set('isGridOn', false);
			// reset the screen size selector
			$('iframe#pf-page-builder-iframe').attr('class', '');
			this.$('select#screen-size-select').val('default');
		}
		// post a message to the app in the iframe
		$('iframe#pf-page-builder-iframe')[0].contentWindow.postMessage({
            property: 'isBuildState',
            value: this.get('isBuildState')
        }, '*');
	}.observes('isBuildState'),

	gridObserver: function() {
		// post a message to the app in the iframe
		$('iframe#pf-page-builder-iframe')[0].contentWindow.postMessage({
            property: 'isGridOn',
            value: this.get('isGridOn')
        }, '*');
	}.observes('isGridOn')
});