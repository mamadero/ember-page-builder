import Ember from 'ember';

export default Ember.Controller.extend({
	needs: ['application'],
	isBuildState: Ember.computed.alias('controllers.application.isBuildState'),
	isGridOn: Ember.computed.alias('controllers.application.isGridOn'),
	isEmbeddedInIframe: Ember.computed.alias('controllers.application.isEmbeddedInIframe'),
	componentToEdit: null,

	iframeSrcUrl: function() {
		return window.location.href + '/embed';
	}.property('model.id'),

	actions: {
		editComponent: function(component) {
			this.set('componentToEdit', component);
		}
	}
});
