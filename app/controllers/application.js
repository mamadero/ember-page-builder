import Ember from 'ember';

export default Ember.Controller.extend({
	isBuildState: true,
	isGridOn: false,
	isEmbeddedInIframe: false,

	onInit: function() {
		// add a window event listener to check for postMessages
		var callback = this.get('postMessageCallback').bind(this);
		window.addEventListener("message", callback, false);
	}.on('init'),

	postMessageCallback: function(event) {
		// only agree to postMessages from the same host
		if (event.origin !== 'http://' + window.location.host &&
			event.origin !== 'https://' + window.location.host) {
			console.log('postMessage host error: not a trusted origin');
			return;
		}
		// set the property defined in the post message
		this.set(event.data.property, event.data.value);
	},

	actions: {
		toggleBuildState: function(state) {
			this.set('isBuildState', state);
		},

		toggleGridDisplay: function(state) {
			this.set('isGridOn', state);
		}
	}
});
