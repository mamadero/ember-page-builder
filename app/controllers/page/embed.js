import Ember from 'ember';

export default Ember.Controller.extend({
	needs: ['page'],
	iframeSrcUrl: Ember.computed.alias('controllers.page.iframeSrcUrl')
});
