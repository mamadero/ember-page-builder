import Ember from 'ember';
import Resolver from 'ember/resolver';

var resolver = Resolver.extend({
    resolveTemplate: function(parsedName) {
        var templateName;

        var defaultTemplate = this._super.apply(this, arguments);
        if (defaultTemplate) {
            return defaultTemplate;
        } else {
            templateName = parsedName.fullNameWithoutType.replace(/\./g, '/');
            if (Ember.TEMPLATES[templateName]) {
                return Ember.TEMPLATES[templateName];
            }

            templateName = Em.String.camelize(templateName);
            if (Ember.TEMPLATES[templateName]) {
                Ember.deprecate('Template name cannot be in camelized form. Consider renaming template ' + templateName + ' to ' + Em.String.dasherize(templateName));
                return Ember.TEMPLATES[templateName];
            }
            try {
                return require('app/templates/' + parsedName.fullNameWithoutType).default;
            } catch (err) {
                return;
            }
        }
    }
});

export default resolver;