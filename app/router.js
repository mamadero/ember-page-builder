import Ember from 'ember';
import config from './config/environment';

var Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
    this.resource('page', { path: '/:page_id' }, function() {
      this.route('embed');
    });
});

export default Router;
